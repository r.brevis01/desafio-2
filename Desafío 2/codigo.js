var btn0 = document.getElementById("nro0");
var btn1 = document.getElementById("nro1");
var btn2 = document.getElementById("nro2");
var btn3 = document.getElementById("nro3");
var btn4 = document.getElementById("nro4");
var btn5 = document.getElementById("nro5");
var btn6 = document.getElementById("nro6");
var btn7 = document.getElementById("nro7");
var btn8 = document.getElementById("nro8");
var btn9 = document.getElementById("nro9");
var btnSuma = document.getElementById("suma");
var btnResta = document.getElementById("resta");
var btnMultiplicacion = document.getElementById("multi");
var btnDivision = document.getElementById("division");
var btnSumRes = document.getElementById("sumres");
var btnBorrar = document.getElementById("borrar");
var btnclean = document.getElementById("clean");
var btnDecima = document.getElementById("decimal");
var btnResultado = document.getElementById("resultado");
var seccionGurdado = document.getElementById("nroGuardado");
var seccionOperacion = document.getElementById("operacion");
var seccionActivo = document.getElementById("numeroactivo");






btn1.addEventListener("click", function() {

    var nro1 = btn1.value;

    seccionActivo.innerHTML = seccionActivo.textContent + nro1;

});

btn2.addEventListener("click", function() {

    var nro2 = btn2.value;

    seccionActivo.innerHTML = seccionActivo.textContent + nro2;

});
btn3.addEventListener("click", function() {

    var nro3 = btn3.value;

    seccionActivo.innerHTML = seccionActivo.textContent + nro3;

});
btn4.addEventListener("click", function() {

    var nro4 = btn4.value;

    seccionActivo.innerHTML = seccionActivo.textContent + nro4;

});
btn5.addEventListener("click", function() {

    var nro5 = btn5.value;

    seccionActivo.innerHTML = seccionActivo.textContent + nro5;

});
btn6.addEventListener("click", function() {

    var nro6 = btn6.value;

    seccionActivo.innerHTML = seccionActivo.textContent + nro6;

});

btn7.addEventListener("click", function() {

    var nro7 = btn7.value;

    seccionActivo.innerHTML = seccionActivo.textContent + nro7;

});
btn8.addEventListener("click", function() {

    var nro8 = btn8.value;

    seccionActivo.innerHTML = seccionActivo.textContent + nro8;

});
btn9.addEventListener("click", function() {

    var nro9 = btn9.value;

    seccionActivo.innerHTML = seccionActivo.textContent + nro9;

});
btn0.addEventListener("click", function() {

    var nro0 = btn0.value;

    seccionActivo.innerHTML = seccionActivo.textContent + nro0;

});

btnDecima.addEventListener("click", function() {

    var nrodecimal = btnDecima.value;

    if (contadorDecimal(seccionActivo.textContent) > 0) {
        seccionActivo.innerHTML = seccionActivo.textContent;


    } else {
        seccionActivo.innerHTML = seccionActivo.textContent + nrodecimal;
    }
});

btnclean.addEventListener("click", function() {
    seccionActivo.innerHTML = botonClean();
    seccionGurdado.innerHTML = botonClean();
    seccionOperacion.innerHTML = botonClean();

});


btnSuma.addEventListener("click", function() {
    if (seccionActivo == undefined) {
        seccionActivo.innerHTML = botonClean();


    } else if (seccionActivo != undefined) {
        seccionGurdado.innerHTML = seccionActivo.textContent;

        seccionActivo.innerHTML = botonClean();
        seccionOperacion.innerHTML = btnSuma.value;
    }

});

btnResta.addEventListener("click", function() {
    if (seccionActivo == undefined) {
        seccionActivo.innerHTML = botonClean();

    } else if (seccionActivo != undefined) {
        seccionGurdado.innerHTML = seccionActivo.textContent;

        seccionActivo.innerHTML = botonClean();
        seccionOperacion.innerHTML = btnResta.value;
    }

});

btnMultiplicacion.addEventListener("click", function() {
    if (seccionActivo == undefined) {
        seccionActivo.innerHTML = botonClean();

    } else if (seccionActivo != undefined) {
        seccionGurdado.innerHTML = seccionActivo.textContent;

        seccionActivo.innerHTML = botonClean();
        seccionOperacion.innerHTML = btnMultiplicacion.value;
    }

});

btnDivision.addEventListener("click", function() {
    if (seccionActivo == undefined) {
        seccionActivo.innerHTML = botonClean();

    } else if (seccionActivo != undefined) {
        seccionGurdado.innerHTML = seccionActivo.textContent;

        seccionActivo.innerHTML = botonClean();
        seccionOperacion.innerHTML = btnDivision.value;
    }

});


btnResultado.addEventListener("click", function() {
    var nro1 = seccionGurdado.textContent;
    var operacion = seccionOperacion.textContent;
    var nro2 = seccionActivo.textContent;

    if (botonResultado(operacion, nro1, nro2) == "error") {

        seccionActivo.innerHTML = botonResultado(operacion, nro1, nro2).fontcolor("red");


    } else {
        seccionActivo.innerHTML = botonResultado(operacion, nro1, nro2);
    }

});

btnBorrar.addEventListener("click", function() {

    var nro1 = seccionActivo.textContent;

    seccionActivo.innerHTML = nro1.replace(/.$/, '');
});

btnSumRes.addEventListener("click", function() {
    seccionActivo.innerHTML = parseFloat(seccionActivo.textContent) * -1;

});


function botonBorrar(nroBorrar) {
    return nroBorrar.replace(/.$/, "");
}

function botonClean() {
    return "";
}


function botonResultado(operacion, nroguard, nroacti) {

    if (operacion == "+") {

        return parseFloat(nroguard) + parseFloat(nroacti);

    } else if (operacion == "-") {
        return parseFloat(nroguard) - parseFloat(nroacti);
    } else if (operacion == "*") {
        return parseFloat(nroguard) * parseFloat(nroacti);
    } else if (operacion == "/") {
        if (nroguard == "0" || nroacti == "0") {
            return "error";
        } else {
            return parseFloat(nroguard) / parseFloat(nroacti);
        }


    }

}



function contadorDecimal(nro) {
    var indices = [];
    for (var i = 0; i < nro.length; i++) {
        if (nro[i].toLowerCase() === ".") indices.push(i);
    }
    return indices.length;
}